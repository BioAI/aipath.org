# AIPATH.ORG Website Source Code 
This website is available at [aipath.org](https://aipath.org/)

## How-tos 
- Configure the naviation bar: do this at `_data/naviation.yml`.

## Credit 
- Jekyll 
- [Hydra theme by CloudCanon](https://github.com/CloudCannon/hydra-jekyll-template)

## Content license
Copyright (c) by Xi'an Jiaotong University